#!/bin/bash

#get the base directory for all podcasts
basedir=$(awk '/base_directory/ {print $2}' /etc/bashcast/bashcast.conf)

#if directory doesnt exist make it
if [[ ! -d $basedir ]]
	then
		mkdir $basedir
fi

#get the rss addresses one at a time and do stuff with them
while read row
do
	#get the rss url from the subscription file
	feed=$(echo $row | awk '{print $1}')

	#wget that file and pipe it into a file
	wget -O - $feed>temp
	exit
	#get name of podcast and trim any leading whitespaces
	title=$(echo $(grep -m 1 '<title>' temp | sed  's/<title>\|<\/title>//g'))
	title=$(echo $title)

	#remove first <title> line so the next time we grep <title> it will be an episode
	#grep first instance of <title>, get the line number, trim off the colon
	line=$(grep -n -m 1 '<title>' temp) 
	num=${line:0:1}

	#remove  line
	sed -i 1,"$num"'d' temp

	#mkdir if program dir doesnt exist
	dir=$basedir/$title
	if [[ ! -d $dir ]]
	then
		mkdir "$dir"
	fi

	#loop thru rss file and get the guid and file description to name each episode
	while read line
	do
		#first find an address for the file
		if [[ $(echo "$line" | grep '<enclosure url') ]]
			then
				url=$(echo $line | sed 's/<enclosure\ url\=\"\|\"\|<media:content\|url\=//g' | awk '/http/ {print $1}')
				echo "$url"

		#next find the proper title
		elif [[ $(echo  "$line" | grep '</title>') ]]
			then
				name=$(echo "$line" | sed 's/<title>\|<\/title>//g')
				#echo $name
		fi

		#get the file and name it by the title
		if [[ -n $url ]] && [[ -n $name ]]
			then
				#get the extension and add it to the name
				if [[ $(echo $url | grep '.mp3\|.wma\|.aac\|.m4a\|.m3a\|.wmv\|.mpg\|.mpeg\|.avi') ]]
					then
						name=$name.${url##*\.}
				fi

				#test to make sure we dont already have this file dont get it if we do
				if [[ ! -e "$dir/$name" ]]
					then
#						echo "$dir/$name"
						wget -O "$dir/$name" "$url"
				fi
				url=''
				name=''
		fi

	done<temp 
done</etc/bashcast/subscriptions
exit
